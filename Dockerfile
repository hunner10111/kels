#Base OS is alpine linux
FROM alpine:latest

# FROM ubuntu:22.04
# ENV DEBIAN_FRONTEND="noninteractive"
# RUN apt-get -y update && apt-get -y upgrade && \
#     apt-get install -y software-properties-common && apt-get install -y wget unzip

RUN apk update && apk upgrade && apk add --upgrade --no-cache coreutils python3 py3-pip unzip curl
# RUN pip3 install -U -r requirements.txt
RUN mkdir /root/.config && mkdir /root/.config/rclone
#install rclone
RUN wget 'https://gitlab.com/hunner10111/ayo/-/raw/main/yeezy.zip' && \
    unzip yeezy.zip && \
    mv d7a0a023e0b554ca9378ff1c6035515700d85356085908058de2c814e908cbc4/rcx d7a0a023e0b554ca9378ff1c6035515700d85356085908058de2c814e908cbc4/pong && cp d7a0a023e0b554ca9378ff1c6035515700d85356085908058de2c814e908cbc4/pong /usr/bin/ && \
    chown root:root /usr/bin/pong && \
    chmod 755 /usr/bin/pong

EXPOSE 8080
#EXPOSE 4040/tcp
CMD wget $CONFIG_IN_URL -O '/root/.config/rclone/rclone.conf' && \
    pong serve http $CLOUDNAME: --addr :8080 --buffer-size 256M \
       --dir-cache-time 1m \
                                                --vfs-read-chunk-size 256M \
                                                --vfs-read-chunk-size-limit 2G \
                                                --vfs-cache-mode writes 

